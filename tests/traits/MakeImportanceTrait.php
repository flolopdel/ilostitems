<?php

use Faker\Factory as Faker;
use App\Models\Importance;
use App\Repositories\ImportanceRepository;

trait MakeImportanceTrait
{
    /**
     * Create fake instance of Importance and save it in database
     *
     * @param array $importanceFields
     * @return Importance
     */
    public function makeImportance($importanceFields = [])
    {
        /** @var ImportanceRepository $importanceRepo */
        $importanceRepo = App::make(ImportanceRepository::class);
        $theme = $this->fakeImportanceData($importanceFields);
        return $importanceRepo->create($theme);
    }

    /**
     * Get fake instance of Importance
     *
     * @param array $importanceFields
     * @return Importance
     */
    public function fakeImportance($importanceFields = [])
    {
        return new Importance($this->fakeImportanceData($importanceFields));
    }

    /**
     * Get fake data of Importance
     *
     * @param array $postFields
     * @return array
     */
    public function fakeImportanceData($importanceFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $importanceFields);
    }
}
