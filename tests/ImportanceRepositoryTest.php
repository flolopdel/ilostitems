<?php

use App\Models\Importance;
use App\Repositories\ImportanceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImportanceRepositoryTest extends TestCase
{
    use MakeImportanceTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ImportanceRepository
     */
    protected $importanceRepo;

    public function setUp()
    {
        parent::setUp();
        $this->importanceRepo = App::make(ImportanceRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateImportance()
    {
        $importance = $this->fakeImportanceData();
        $createdImportance = $this->importanceRepo->create($importance);
        $createdImportance = $createdImportance->toArray();
        $this->assertArrayHasKey('id', $createdImportance);
        $this->assertNotNull($createdImportance['id'], 'Created Importance must have id specified');
        $this->assertNotNull(Importance::find($createdImportance['id']), 'Importance with given id must be in DB');
        $this->assertModelData($importance, $createdImportance);
    }

    /**
     * @test read
     */
    public function testReadImportance()
    {
        $importance = $this->makeImportance();
        $dbImportance = $this->importanceRepo->find($importance->id);
        $dbImportance = $dbImportance->toArray();
        $this->assertModelData($importance->toArray(), $dbImportance);
    }

    /**
     * @test update
     */
    public function testUpdateImportance()
    {
        $importance = $this->makeImportance();
        $fakeImportance = $this->fakeImportanceData();
        $updatedImportance = $this->importanceRepo->update($fakeImportance, $importance->id);
        $this->assertModelData($fakeImportance, $updatedImportance->toArray());
        $dbImportance = $this->importanceRepo->find($importance->id);
        $this->assertModelData($fakeImportance, $dbImportance->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteImportance()
    {
        $importance = $this->makeImportance();
        $resp = $this->importanceRepo->delete($importance->id);
        $this->assertTrue($resp);
        $this->assertNull(Importance::find($importance->id), 'Importance should not exist in DB');
    }
}
