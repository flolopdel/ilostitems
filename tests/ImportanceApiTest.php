<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImportanceApiTest extends TestCase
{
    use MakeImportanceTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateImportance()
    {
        $importance = $this->fakeImportanceData();
        $this->json('POST', '/api/v1/importances', $importance);

        $this->assertApiResponse($importance);
    }

    /**
     * @test
     */
    public function testReadImportance()
    {
        $importance = $this->makeImportance();
        $this->json('GET', '/api/v1/importances/'.$importance->id);

        $this->assertApiResponse($importance->toArray());
    }

    /**
     * @test
     */
    public function testUpdateImportance()
    {
        $importance = $this->makeImportance();
        $editedImportance = $this->fakeImportanceData();

        $this->json('PUT', '/api/v1/importances/'.$importance->id, $editedImportance);

        $this->assertApiResponse($editedImportance);
    }

    /**
     * @test
     */
    public function testDeleteImportance()
    {
        $importance = $this->makeImportance();
        $this->json('DELETE', '/api/v1/importances/'.$importance->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/importances/'.$importance->id);

        $this->assertResponseStatus(404);
    }
}
