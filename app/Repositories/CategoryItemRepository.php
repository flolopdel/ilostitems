<?php

namespace App\Repositories;

use App\Models\CategoryItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CategoryItemRepository
 * @package App\Repositories
 * @version November 19, 2017, 2:29 pm UTC
 *
 * @method CategoryItem findWithoutFail($id, $columns = ['*'])
 * @method CategoryItem find($id, $columns = ['*'])
 * @method CategoryItem first($columns = ['*'])
*/
class CategoryItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'category_id',
        'item_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryItem::class;
    }
}
