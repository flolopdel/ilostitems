<?php

namespace App\Repositories;

use App\Models\Importance;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ImportanceRepository
 * @package App\Repositories
 * @version November 19, 2017, 2:12 pm UTC
 *
 * @method Importance findWithoutFail($id, $columns = ['*'])
 * @method Importance find($id, $columns = ['*'])
 * @method Importance first($columns = ['*'])
*/
class ImportanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Importance::class;
    }
}
