<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategoryItemAPIRequest;
use App\Http\Requests\API\UpdateCategoryItemAPIRequest;
use App\Models\CategoryItem;
use App\Repositories\CategoryItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CategoryItemController
 * @package App\Http\Controllers\API
 */

class CategoryItemAPIController extends AppBaseController
{
    /** @var  CategoryItemRepository */
    private $categoryItemRepository;

    public function __construct(CategoryItemRepository $categoryItemRepo)
    {
        $this->categoryItemRepository = $categoryItemRepo;
    }

    /**
     * Display a listing of the CategoryItem.
     * GET|HEAD /categoryItems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->categoryItemRepository->pushCriteria(new RequestCriteria($request));
        $this->categoryItemRepository->pushCriteria(new LimitOffsetCriteria($request));
        $categoryItems = $this->categoryItemRepository->all();

        return $this->sendResponse($categoryItems->toArray(), 'Category Items retrieved successfully');
    }

    /**
     * Store a newly created CategoryItem in storage.
     * POST /categoryItems
     *
     * @param CreateCategoryItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryItemAPIRequest $request)
    {
        $input = $request->all();

        $categoryItems = $this->categoryItemRepository->create($input);

        return $this->sendResponse($categoryItems->toArray(), 'Category Item saved successfully');
    }

    /**
     * Display the specified CategoryItem.
     * GET|HEAD /categoryItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CategoryItem $categoryItem */
        $categoryItem = $this->categoryItemRepository->findWithoutFail($id);

        if (empty($categoryItem)) {
            return $this->sendError('Category Item not found');
        }

        return $this->sendResponse($categoryItem->toArray(), 'Category Item retrieved successfully');
    }

    /**
     * Update the specified CategoryItem in storage.
     * PUT/PATCH /categoryItems/{id}
     *
     * @param  int $id
     * @param UpdateCategoryItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryItemAPIRequest $request)
    {
        $input = $request->all();

        /** @var CategoryItem $categoryItem */
        $categoryItem = $this->categoryItemRepository->findWithoutFail($id);

        if (empty($categoryItem)) {
            return $this->sendError('Category Item not found');
        }

        $categoryItem = $this->categoryItemRepository->update($input, $id);

        return $this->sendResponse($categoryItem->toArray(), 'CategoryItem updated successfully');
    }

    /**
     * Remove the specified CategoryItem from storage.
     * DELETE /categoryItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CategoryItem $categoryItem */
        $categoryItem = $this->categoryItemRepository->findWithoutFail($id);

        if (empty($categoryItem)) {
            return $this->sendError('Category Item not found');
        }

        $categoryItem->delete();

        return $this->sendResponse($id, 'Category Item deleted successfully');
    }
}
