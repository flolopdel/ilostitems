<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateImportanceAPIRequest;
use App\Http\Requests\API\UpdateImportanceAPIRequest;
use App\Models\Importance;
use App\Repositories\ImportanceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ImportanceController
 * @package App\Http\Controllers\API
 */

class ImportanceAPIController extends AppBaseController
{
    /** @var  ImportanceRepository */
    private $importanceRepository;

    public function __construct(ImportanceRepository $importanceRepo)
    {
        $this->importanceRepository = $importanceRepo;
    }

    /**
     * Display a listing of the Importance.
     * GET|HEAD /importances
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->importanceRepository->pushCriteria(new RequestCriteria($request));
        $this->importanceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $importances = $this->importanceRepository->all();

        return $this->sendResponse($importances->toArray(), 'Importances retrieved successfully');
    }

    /**
     * Store a newly created Importance in storage.
     * POST /importances
     *
     * @param CreateImportanceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateImportanceAPIRequest $request)
    {
        $input = $request->all();

        $importances = $this->importanceRepository->create($input);

        return $this->sendResponse($importances->toArray(), 'Importance saved successfully');
    }

    /**
     * Display the specified Importance.
     * GET|HEAD /importances/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Importance $importance */
        $importance = $this->importanceRepository->findWithoutFail($id);

        if (empty($importance)) {
            return $this->sendError('Importance not found');
        }

        return $this->sendResponse($importance->toArray(), 'Importance retrieved successfully');
    }

    /**
     * Update the specified Importance in storage.
     * PUT/PATCH /importances/{id}
     *
     * @param  int $id
     * @param UpdateImportanceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateImportanceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Importance $importance */
        $importance = $this->importanceRepository->findWithoutFail($id);

        if (empty($importance)) {
            return $this->sendError('Importance not found');
        }

        $importance = $this->importanceRepository->update($input, $id);

        return $this->sendResponse($importance->toArray(), 'Importance updated successfully');
    }

    /**
     * Remove the specified Importance from storage.
     * DELETE /importances/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Importance $importance */
        $importance = $this->importanceRepository->findWithoutFail($id);

        if (empty($importance)) {
            return $this->sendError('Importance not found');
        }

        $importance->delete();

        return $this->sendResponse($id, 'Importance deleted successfully');
    }
}
