<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateImportanceRequest;
use App\Http\Requests\UpdateImportanceRequest;
use App\Repositories\ImportanceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ImportanceController extends AppBaseController
{
    /** @var  ImportanceRepository */
    private $importanceRepository;

    public function __construct(ImportanceRepository $importanceRepo)
    {
        $this->importanceRepository = $importanceRepo;
    }

    /**
     * Display a listing of the Importance.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->importanceRepository->pushCriteria(new RequestCriteria($request));
        $importances = $this->importanceRepository->all();

        return view('importances.index')
            ->with('importances', $importances);
    }

    /**
     * Show the form for creating a new Importance.
     *
     * @return Response
     */
    public function create()
    {
        return view('importances.create');
    }

    /**
     * Store a newly created Importance in storage.
     *
     * @param CreateImportanceRequest $request
     *
     * @return Response
     */
    public function store(CreateImportanceRequest $request)
    {
        $input = $request->all();

        $importance = $this->importanceRepository->create($input);

        Flash::success('Importance saved successfully.');

        return redirect(route('importances.index'));
    }

    /**
     * Display the specified Importance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $importance = $this->importanceRepository->findWithoutFail($id);

        if (empty($importance)) {
            Flash::error('Importance not found');

            return redirect(route('importances.index'));
        }

        return view('importances.show')->with('importance', $importance);
    }

    /**
     * Show the form for editing the specified Importance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $importance = $this->importanceRepository->findWithoutFail($id);

        if (empty($importance)) {
            Flash::error('Importance not found');

            return redirect(route('importances.index'));
        }

        return view('importances.edit')->with('importance', $importance);
    }

    /**
     * Update the specified Importance in storage.
     *
     * @param  int              $id
     * @param UpdateImportanceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateImportanceRequest $request)
    {
        $importance = $this->importanceRepository->findWithoutFail($id);

        if (empty($importance)) {
            Flash::error('Importance not found');

            return redirect(route('importances.index'));
        }

        $importance = $this->importanceRepository->update($request->all(), $id);

        Flash::success('Importance updated successfully.');

        return redirect(route('importances.index'));
    }

    /**
     * Remove the specified Importance from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $importance = $this->importanceRepository->findWithoutFail($id);

        if (empty($importance)) {
            Flash::error('Importance not found');

            return redirect(route('importances.index'));
        }

        $this->importanceRepository->delete($id);

        Flash::success('Importance deleted successfully.');

        return redirect(route('importances.index'));
    }
}
