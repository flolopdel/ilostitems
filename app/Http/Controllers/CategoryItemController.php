<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryItemRequest;
use App\Http\Requests\UpdateCategoryItemRequest;
use App\Repositories\CategoryItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CategoryItemController extends AppBaseController
{
    /** @var  CategoryItemRepository */
    private $categoryItemRepository;

    public function __construct(CategoryItemRepository $categoryItemRepo)
    {
        $this->categoryItemRepository = $categoryItemRepo;
    }

    /**
     * Display a listing of the CategoryItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->categoryItemRepository->pushCriteria(new RequestCriteria($request));
        $categoryItems = $this->categoryItemRepository->all();

        return view('category_items.index')
            ->with('categoryItems', $categoryItems);
    }

    /**
     * Show the form for creating a new CategoryItem.
     *
     * @return Response
     */
    public function create()
    {
        return view('category_items.create');
    }

    /**
     * Store a newly created CategoryItem in storage.
     *
     * @param CreateCategoryItemRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryItemRequest $request)
    {
        $input = $request->all();

        $categoryItem = $this->categoryItemRepository->create($input);

        Flash::success('Category Item saved successfully.');

        return redirect(route('categoryItems.index'));
    }

    /**
     * Display the specified CategoryItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoryItem = $this->categoryItemRepository->findWithoutFail($id);

        if (empty($categoryItem)) {
            Flash::error('Category Item not found');

            return redirect(route('categoryItems.index'));
        }

        return view('category_items.show')->with('categoryItem', $categoryItem);
    }

    /**
     * Show the form for editing the specified CategoryItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoryItem = $this->categoryItemRepository->findWithoutFail($id);

        if (empty($categoryItem)) {
            Flash::error('Category Item not found');

            return redirect(route('categoryItems.index'));
        }

        return view('category_items.edit')->with('categoryItem', $categoryItem);
    }

    /**
     * Update the specified CategoryItem in storage.
     *
     * @param  int              $id
     * @param UpdateCategoryItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryItemRequest $request)
    {
        $categoryItem = $this->categoryItemRepository->findWithoutFail($id);

        if (empty($categoryItem)) {
            Flash::error('Category Item not found');

            return redirect(route('categoryItems.index'));
        }

        $categoryItem = $this->categoryItemRepository->update($request->all(), $id);

        Flash::success('Category Item updated successfully.');

        return redirect(route('categoryItems.index'));
    }

    /**
     * Remove the specified CategoryItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoryItem = $this->categoryItemRepository->findWithoutFail($id);

        if (empty($categoryItem)) {
            Flash::error('Category Item not found');

            return redirect(route('categoryItems.index'));
        }

        $this->categoryItemRepository->delete($id);

        Flash::success('Category Item deleted successfully.');

        return redirect(route('categoryItems.index'));
    }
}
