<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Item
 * @package App\Models
 * @version November 19, 2017, 2:23 pm UTC
 *
 * @property string name
 * @property string description
 * @property integer importance_id
 * @property  status_id
 * @property dateTime found_at
 */
class Item extends Model
{
    use SoftDeletes;

    public $table = 'items';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'importance_id',
        'status_id',
        'found_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'importance_id' => 'integer',
        'found_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    /**
     * Get the status.
     */
    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    /**
     * Get the importance.
     */
    public function importance()
    {
        return $this->belongsTo('App\Importance');
    }


}
