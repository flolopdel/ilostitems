<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Status
 * @package App\Models
 * @version November 19, 2017, 2:14 pm UTC
 *
 * @property string name
 */
class Status extends Model
{
    use SoftDeletes;

    public $table = 'statuses';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    /**
     * Get the items for a specific status.
     */
    public function items()
    {
        return $this->hasMany('App\Item');
    }


}
