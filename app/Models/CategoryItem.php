<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CategoryItem
 * @package App\Models
 * @version November 19, 2017, 2:29 pm UTC
 *
 * @property integer category_id
 * @property integer item_id
 */
class CategoryItem extends Model
{
    use SoftDeletes;

    public $table = 'category_items';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'category_id',
        'item_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'category_id' => 'integer',
        'item_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
