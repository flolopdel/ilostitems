<li class="{{ Request::is('importances*') ? 'active' : '' }}">
    <a href="{!! route('importances.index') !!}"><i class="fa fa-edit"></i><span>Importances</span></a>
</li>

<li class="{{ Request::is('statuses*') ? 'active' : '' }}">
    <a href="{!! route('statuses.index') !!}"><i class="fa fa-edit"></i><span>Statuses</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
</li>

<li class="{{ Request::is('items*') ? 'active' : '' }}">
    <a href="{!! route('items.index') !!}"><i class="fa fa-edit"></i><span>Items</span></a>
</li>

<li class="{{ Request::is('categoryItems*') ? 'active' : '' }}">
    <a href="{!! route('categoryItems.index') !!}"><i class="fa fa-edit"></i><span>Category Items</span></a>
</li>

