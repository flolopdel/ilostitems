<table class="table table-responsive" id="importances-table">
    <thead>
        <tr>
            <th>Name</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($importances as $importance)
        <tr>
            <td>{!! $importance->name !!}</td>
            <td>
                {!! Form::open(['route' => ['importances.destroy', $importance->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('importances.show', [$importance->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('importances.edit', [$importance->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>