<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $item->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $item->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $item->description !!}</p>
</div>

<!-- Importance Id Field -->
<div class="form-group">
    {!! Form::label('importance_id', 'Importance Id:') !!}
    <p>{!! $item->importance_id !!}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{!! $item->status_id !!}</p>
</div>

<!-- Found At Field -->
<div class="form-group">
    {!! Form::label('found_at', 'Found At:') !!}
    <p>{!! $item->found_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $item->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $item->updated_at !!}</p>
</div>

