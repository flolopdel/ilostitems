<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Importance Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('importance_id', 'Importance Id:') !!}
    {!! Form::number('importance_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Found At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('found_at', 'Found At:') !!}
    {!! Form::date('found_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('items.index') !!}" class="btn btn-default">Cancel</a>
</div>
