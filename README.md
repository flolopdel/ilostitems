# CMS - LARAVEL
##### Cms created with http://labs.infyom.com/laravelgenerator/ and docker containers

Instalation Infyom:

1. docker exec -ti [NAME] bash
2. composer install
3. npm install
4. php artisan migrate
5. php artisan key:generate
6. php artisan passport:keys

Instalation Passport(Oauth) https://laravel.com/docs/5.5/passport:

1. php artisan migrate
2. php artisan passport:install
3. php artisan vendor:publish --tag=passport-components

Frontend vue https://laravel.com/docs/5.5/frontend

UNIT TEST
1. Create a test in the Feature directory...
php artisan make:test UserTest
2. Create a test in the Unit directory...
php artisan make:test UserTest --unit