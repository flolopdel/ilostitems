<?php


Route::resource('importances', 'ImportanceController');

Route::resource('statuses', 'StatusController');

Route::resource('categories', 'CategoryController');

Route::resource('items', 'ItemController');

Route::resource('categoryItems', 'CategoryItemController');