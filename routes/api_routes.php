<?php




Route::resource('importances', 'ImportanceAPIController');

Route::resource('statuses', 'StatusAPIController');

Route::resource('categories', 'CategoryAPIController');

Route::resource('items', 'ItemAPIController');

Route::resource('category_items', 'CategoryItemAPIController');