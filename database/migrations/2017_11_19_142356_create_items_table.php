<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 256);
            $table->text('description');
            $table->integer('importance_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->dateTime('found_at');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('importance_id')->references('id')->on('importances');
            $table->foreign('status_id')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
